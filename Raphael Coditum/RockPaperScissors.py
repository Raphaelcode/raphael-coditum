from random import randint
import time
timeswon = (0) #This is the variable for how many times the player needs to win so the game can end.
timeslost = (0) #This is the variable for how many times the player needs to lose so the 
while (0 == timeswon or timeswon == 1) and (timeslost == 1 or timeslost == 0): #This while loop the times lost and times won.
    randomgameint = randint(1, 3)
    rock = (1)
    paper = (2)
    scissors = (3)
    print("Do you want to be Rock, Paper, or Scissors?")
    userinput = input().lower()
    player = ""
    if userinput == "rock":
        player = "rock"
    if userinput == "paper":
        player = "paper" 
    if userinput == "scissors":
        player = "scissors"
    print("You are playing as " + player + ".")
    if player == "rock" and randomgameint == 3:
        print("You win!")
        print("Number of times won: " + str(timeswon+1))
        timeswon = timeswon + 1
    elif player == "paper" and randomgameint == 1:
        print("You win!")
        print("Number of times won: " + str(timeswon+1))
        timeswon = timeswon + 1
    elif player == "scissors" and randomgameint == 2:
        print("You win!")
        print("Number of times won: " + str(timeswon+1))
        timeswon = timeswon + 1
    elif player == "rock" and randomgameint == 1:
        print("Tie!")
        print("Number of times won: " + str(timeswon))
        print("Number of times lost: " + str(timeslost))
    elif player == "paper" and randomgameint == 2:
        print("Tie!")
        print("Number of times won: " + str(timeswon))
        print("Number of times lost: " + str(timeslost))
    elif player == "scissors" and randomgameint == 3:
        print("Tie!")
        print("Number of times won: " + str(timeswon))
        print("Number of times lost: " + str(timeslost))
    else:
        print("You lose!")
        print("Number of times lost: " + str(timeslost+1))
        timeslost = timeslost + 1
print("Number of times won: " + str(timeswon))
print("Number of times lost: " + str(timeslost))
if timeswon == 2:
    print("You won best out of three.")
if timeslost == 2:
    print("You lost best out of three.")