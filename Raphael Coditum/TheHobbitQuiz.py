print("Welcome to my Hobbit quiz!")
print("This quiz was made so you can strengthen your knowledge on The Hobbit")
print("You will be given choices on which chapter on The Hobbit you would like to quiz yourself on.")
chapters = []  # This creates the list for the chapters.
chapters.append("1) An Unexpected Party")
chapters.append("2) Roast Mutton")
chapters.append("3) A Short Rest")
chapters.append("4) Overhill and Underhill")
chapters.append("5) Riddles in the Dark")
chapters.append("6) Out of the Frying-Pan and into the fire")
chapters.append("7) Queer Lodgings")
chapters.append("8) Flies and Spiders")
chapters.append("9) Barrels out of Bond")
chapters.append("10) A Warm Welcome")
chapters.append("11) On the Doorstep")
chapters.append("12) Inside Information")
chapters.append("13) Not at home")
chapters.append("14) Fire and Water")
chapters.append("15) The Gathering of the Clouds")
chapters.append("16) A Thief in the Night")
chapters.append("17) The Clouds Burst")
chapters.append("18) The Return Journey")
chapters.append("19) The Last Stage")
firstchapter = [] #This creates the list for the first chapter.
firstchapter.append(["What is Bilbo's ancestry considered by hobbit standards?", "a)Noble", "b)Shameful", "c)Silly", "d)Great", "a"])
firstchapter.append(["When The Hobbit story begins what is Bilbo doing outside front door?", "a)Smoking his pipe", "b)Looking at Clouds", "c)Going to take a walk", "d)Putting a sign up", "a"])
firstchapter.append(["While Bilbo is enjoying his pipe who arrives at his house?", "a)His neighbor", "b)Gandalf", "c)Thorin", "d)Dwalin", "b"])
firstchapter.append(["Gandalf asks Bilbo if he would like to", "a)Have lunch with him.b)Have tea.c)Smoke a pipe with him.d)Go on an adventure.", "d"])
firstchapter.append(["Who visits Bilbo the day after Gandalf invited Bilbo to go on an adventure?", "a)Bilbo's uncle", "b)Goblin-king", "c)Gandalf and the Dwarves", "d)Beorn", "c"])
firstchapter.append(["What did Gandalf volunteer Bilbo to be for the dwarves on their adventure?", "a)Cook", "b)Smith", "c)Tracker", "d)Burglar", "d"])
firstchapter.append(["What does Gandalf point to after he pulls out an old map of a great mountain?", "a)The dragon Smaug", "b)How old the map was", "c)The main entrance", "d)A mysterious secret entrance", "d"])
firstchapter.append(["Who is the head dwarf?", "a)Dori", "b)Thorin", "c)Dwalin", "d)Balin", "b"])
firstchapter.append(["Who was Thror?", "a)Bilbo's uncle", "b)King under the Mountain", "c)Bilbo's grandfather", "d)A thief", "b"])
firstchapter.append(["What did Smaug do?", "a)Scattered and killed all of Thror's people.", "b)Moved into the Lonely Mountain", "c)Asked Thror for help.", "d)Befriended Thror.", "a"])
firstchapter.append(["Who held the key to the secret entrance?", "a)Gandalf", "b)Thorin", "c)Balin", "d)Beorn", "a"])
firstchapter.append(["How many dwarves are in Thorin's company?", "a)41", "b)87", "c)13", "d)92", "c"])
secondchapter = [] #This creates the list for the second chapter.
secondchapter.append(["When Bilbo wakes up he realizes that the dwarves", "a)Made breakfast for Bilbo", "b)Talking with Gandalf", "c)Left without him", "d)Were singing a song", "c"])
secondchapter.append(["Which lands did Thorin and his company find themselves far into?", "a)Lone-lands", "b)Dale", "c)Misty Mountains", "d)Erebor", "a"])
secondchapter.append(["What does Thorin and his company see in the distance?", "a)A Fire", "b)An animal", "c)Another troll", "d)A man", "a"])
secondchapter.append(["What does Bilbo see as he approaches a clearing in the woods?", "a)Rabbits", "b)Goblin", "c)Other dwarves", "d)Trolls", "d"])
secondchapter.append(["What are the trolls eating?", "a)Carrots", "b)Mutton", "c)Bread", "d)Potatoes", "b"])
secondchapter.append(["What does Bilbo try to steal from the Trolls?", "a)Potatoes", "b)Weapons", "c)A purse", "d)A map", "c"])
secondchapter.append(["The Trolls were fighting about", "a)How to befriend Bilbo", "b)How they were going to to eat Bilbo", "c)How to interrogate Bilbo","d)What to do with Bilbo", "c"])
secondchapter.append(["After the Trolls stop fighting what do they do to the dwarves?", "a)They greet the dwarves", "b)They throw a sack on each approaching dwarf", "c)They drop Bilbo and run away","d)They attack the dwarves", "b"])
secondchapter.append(["What do the trolls decide to do?", "a)To become friends with the dwarves", "b)To cook the dwarves", "c)To make the dwarves leaved)To join Thorin's company", "b"])
secondchapter.append(["What happens when the Sun peeks over the horizon?", "a)The trolls run away", "b)The trolls calm down", "c)The trolls freeze", "d)The trolls fall asleep", "c"])
secondchapter.append(["What did Gandalf do to mislead the dwarves and keep the trolls arguing all morning?", "a)He had been throwing his voice", "b)He was having tea","c)He was taking a nap","d)He was eating", "a"])
secondchapter.append(["What does Thorin and his company find after searching nearby?", "a)They find a troll's cave", "b)They find a path", "c)They find a big rock", "d)They find another forest", "a"])
secondchapter.append(["What do Gandalf, Thorin, and Bilbo each take from the troll cave?", "a)Food", "b)A sword", "c)A carrot", "d)A potato", "b"])
secondchapter.append(["How long do Thorin and his company sleep?", "a)Early morning", "b)Until the afternoon", "c)Until late morning", "d)Until noon", "b"])
secondchapter.append(["What do trolls need to hide in from the sun?", "a)A house", "b)A mountain", "c)A tree", "d)A cave or hole"])
thirdchapter = [] #This creates the list for the third chapter.
thirdchapter.append(["Which direction did Thorin's company journey after the troll incident?", "a)Eastward", "b)Westward", "c)North", "d)South", "a"])
thirdchapter.append(["What did Gandalf inform the company?", "a)That they were approaching the hidden valley of Rivendell", "b)They were approaching the Lonely Mountain", "c)They were approaching Dale", "d)They were approaching Laketown", "a"])
thirdchapter.append(["What did Gandalf tell his Elf-friends?", "a)To protect them", "b)To except their arrival", "c)To get food ready", "d)To give them a warm welcome", "b"])
thirdchapter.append(["How did Thorin and his company get into Rivendell?", "a)Through a zig-zag path", "b)Through a steep path", "c)On a hill", "d)Inside a mountain", "a"])
thirdchapter.append(["Where did the company of Thorin at last come to?", "a)Erebor", "b)Dale", "c)Laketown", "d)The Last Homely House", "d"])
thirdchapter.append(["What is another name for Orcrist?", "a)Foe-hammer", "b)Orc sword", "c)The Goblin-cleaver", "d)Goblin-hammer", "c"])
thirdchapter.append(["What did Elrond find on Thror's map after he inspected it?", "a)A dragon", "b)A mountain", "c)Moon letters", "d)The secret entrance", "c"])
thirdchapter.append(["When did Thorin and his company depart from Rivendell?", "a)Durin's day", "b)Mid-year's day", "c)Mersday", "d)Highday", "b"])
fourthchapter = [] #This creates the list for the fourth chapter.
fourthchapter.append(["Who was hurling rocks as a game?", "a)Thorin", "b)Gandalf", "c)Smaug", "d)Stone Giants", "d"])
fourthchapter.append(["What was Thorin annoyed by?", "a)How long the journey was taking.", "b)The place the Stone Giants were playing the game.", "c)Bilbo kept complaining.", "d)The way Gandalf was taking them.", "b"])
fourthchapter.append(["What were Fili and Kili sent to do?", "a)Find better shelter.", "b)To find more food", "c)To scout the terrain.", "d)To move rocks out of the path", "a"])
fourthchapter.append(["How did Gandalf fully explore the cave?", "a)He lit his wand to fully explore the cave.", "b)He saw something in the corner of the cave.", "c)He heard something.", "d)He didn't want the company to get ambushed.", "a"])
fourthchapter.append(["What was Bilbo troubled by?", "a)If the quest would fail", "b)Nasty Dreams", "c)Surviving the journey", "d)The Mountains", "b"])
fourthchapter.append(["What happened at the back of the cave?", "a)Someone attacked the company" , "b)A wide crack", "c)One of the ponies got scared", "d)A part of the roof fell in", "b"])
fourthchapter.append(["What started swarming the cave?", "a)Goblins", "b)Bats", "c)More dwarves", "d)Wolves", "a"])
fourthchapter.append(["Who were Bilbo and the Dwarves interrogated by?", "a)Bard", "b)Thror", "c)Smaug", "d)The Great Goblin", "d"])
fourthchapter.append(["Who were the dwarves and Bilbo rescued by?", "a)Lord Elrond", "b)Gandalf", "c)Troll", "d)Thror", "b"])
fifthchapter = []
fifthchapter.append(["What does Bilbo realize when he wakes up in the dark Goblin tunnels?", "a)Gandalf and the dwarves are standing over him", "b)He is all alone", "c)He is outside of the cave", "d)There is food next to him", "b"])
fifthchapter.append(["What does Bilbo find?", "a)A little metal ring", "b)Food", "c)Water", "d)Another sword", "a"])
fifthchapter.append(["What is Bilbo blocked by in the path?", "a)A big rock", "b)A goblin", "c)An icy pool", "d)A chasm", "c"])
fifthchapter.append(["Who does Bilbo meet?", "a)Thror", "b)Gollum", "c)Bard", "d)Lord Elrond", "b"])
fifthchapter.append(["What does Gollum challenge Bilbo to?", "a)A Game of Riddles", "b)A fight", "c)A test", "d)A race", "a"])
fifthchapter.append(["What does Gollum want to do to Bilbo?", "a)Befriend him", "b)Help him escape", "c)Eat him", "d)Steal his weapon", "c"])
fifthchapter.append(["What does Bilbo do when Gollum is chasing him?", "a)He fights Gollum", "b)He gives the ring to Gollum", "c)Gollum knocks him unconcious", "d)Bilbo runs up the tunnel and slips the ring unto his finger.", "d"])
fifthchapter.append(["When Bilbo gets to the end of the tunnel he is relieved to see real sunlight\nbut then he forgets his joy when", "a)He sees that Gollum is still around", "b)He sees that the door is half covered with a big rock.", "c)He sees that there is a troll at the door", "d)He sees armored Goblins guarding the door.", "d"])
print("")
whileloopvar=(0)
while whileloopvar==whileloopvar: #This loops the program forever.
    for chapterloop in range (18): #This loop repeats until all the chapters are printed.
        print(chapters[chapterloop])
    print("")
    print("Choose your subject")
    chapterchoice = input()
    
    if chapterchoice == "1":
        chapter = firstchapter
    if chapterchoice == "2":
        chapter = secondchapter
    if chapterchoice == "3":
        chapter = thirdchapter
    if chapterchoice == "4":
        chapter = fourthchapter
    if chapterchoice == "5":
        chapter = fifthchapter
    subjectanswerloop = False
    for firstchapterloop in range(len(chapter)):
        subjectanswerloop = False
        while not subjectanswerloop:   
            print(chapter[firstchapterloop][0])
            print(chapter[firstchapterloop][1])
            print(chapter[firstchapterloop][2])
            print(chapter[firstchapterloop][3])
            print(chapter[firstchapterloop][4])
            subjectanswer1 = input()
            if subjectanswer1 == chapter[firstchapterloop][5]:
                print("Correct!")
                subjectanswerloop = True
            else:
                if subjectanswer1 == "a":
                    chapter[firstchapterloop][1] = ""
                if subjectanswer1 == "b":
                    chapter[firstchapterloop][2] = ""
                if subjectanswer1 == "c":
                    chapter[firstchapterloop][3] = ""
                if subjectanswer1 == "d":
                    chapter[firstchapterloop][4] = ""