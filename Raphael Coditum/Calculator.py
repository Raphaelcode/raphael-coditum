repeat="Y"
while repeat=="Y" or repeat=="y":
    print("Choose subtraction or addition. Type - for subtraction, + for addition, / for division, and * for multiplication.")
    choice=input()
    print("Insert first number")
    first=int(input())
    print("Insert second number")
    second=int(input())
    if choice==("-"):
        print(first - second)
    elif choice==("+"):
        print(first + second)
    elif choice==("/"):
        print(first / second)
    elif choice==("*"):
        print(first * second)
    else:
        print("You input the wrong number.")
    print("Would you like to make another calculation? Type Y for yes and N for no.")
    repeat=input()