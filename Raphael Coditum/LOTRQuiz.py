from random import randint
print("=-=-=Welcome to my Lord of the Rings/Silmarillion quiz!=-=-=")
print("=-=-=There are 20 Lord of the Rings and Silmarillion questions.=-=-=")
print("=-=-=The quiz will start with a Silmarillion question.=-=-=")
print("=-=-=After that the quiz will give you a Lord of the Rings(LOTR) question=-=-=")
print("=-=-=Then the pattern of questions asked will repeat until the quiz is completed.=-=-=")
lotrquiz = []  # This creates the list for the LOTR questions.
lotrquiz.append(["Silmarillion: Who was the first High King of the Noldor?", "a)Finarfin\nb)Feanor\nc)Finwe\nd)Fingolfin", "c"])  # All these lines contain the questions for the LOTR quiz. #1
lotrquiz.append(["LOTR: Who was the master of Rivendell?", "a)Elros\nb)Gil-Galad\nc)Earendil\nd)Elrond", "d"])  # 2
lotrquiz.append(["Silmarillion: Who was known as the beginning of evil and the enemy of Beleriand.", "a)Elwing\nb)Tulkas\nc)Morgoth\nd)Arwen", "c"])  # 3
lotrquiz.append(["LOTR: Who is the daughter of Lord Elrond?", "a)Tulkas\nb)Arwen\nc)Gimli\nd)Frodo", "b"])  # 4
lotrquiz.append(["Silmarillion: Who is the Chief of the Valar?", "a)Manwe\nb)Earendil\nc)Aragorn\nd)Thingol", "a"])  # 5
lotrquiz.append(["LOTR: Who is Isildur's Heir?", "a)Aragorn\nb)Celebrimbor\nc)Thorin\nd)Bilbo", "a"])  # 6
lotrquiz.append(["Silmarillion: Who was the leader of the Noldor rebellion and the creator of the Silmarils.", "a)Celebrimbor\nb)Feanor\nc)Ulmo\nd)Gil-Galad", "b"])  # 7
lotrquiz.append(["LOTR: Who was chosen to take the ring to Mordor in the Council of Elrond.", "a)Frodo\nb)Samwise\nc)Arwen\nd)Tulkas", "a"])  # 8
lotrquiz.append(["Silmarillion: What was the fourth great battle of the Wars of Beleriand.", "a)Nirnaeth Arnoediad\nb)Dagor Bragollach\nc)Siege of Minas Tirith\ndå)Battle of Dagorlad", "b"])  # 9
lotrquiz.append(["LOTR: Who was leader of the Nazgul?", "a)Tulkas\nb)Earendil\nc)Witch King of Angmar\nd)Maeglin", "c"])  # 10
lotrquiz.append(["Silmarillion: Who betrayed Gondolin by giving the location to Morgoth?", "a)Orome\nb)Legolas\nc)Frodo\nd)Maeglin", "d"])  # 11
lotrquiz.append(["LOTR: Who is the main enemy in The Lord of the Rings.", "a)Azog\nb)Dain\nc)Hurin\nd)Sauron", "d"])  # 12
lotrquiz.append(["Silmarillion: Who is the father of Turin?", "a)Curufin\nb)Gandalf\nc)Hurin\nd)Turgon", "c"])  # 13
lotrquiz.append(["LOTR: Who was Frodos' companion that followed Frodo all the way into Mount Doom?", "a)Samwise\nb)Fingon\nc)Bolg\nd)Frodo", "a"])  # 14
lotrquiz.append(["Silmarillion: What is the name of the Kings' Mountain in Numenor?", "a)Meneltarma\nb)Gondolin\nc)Thangorodrim\nd)Barad-Dur", "a"])  # 15
lotrquiz.append(["LOTR: Who is the lord of Dol Amroth?", "a)Fingolfin\nb)Prince Imrahil\nc)Isildur\nd)Turgon", "b"])  # 16
lotrquiz.append(["Silmarillion: Who challenged Morgoth to a duel in hand to hand combat on front of Angband?", "a)Gimli\nb)Dunharrow\nc)Fingolfin\nd)Ulmo", "c"])  # 17
lotrquiz.append(["LOTR: What is the name of the place that the army of the Rohirrim was marshaled.", "a)Dale\nb)Erebor\nc)Dunharrow\nd)Minas Tirith", "c"])  # 18
lotrquiz.append(["Silmarillion: What is Morgoth's real name?", "a)Ulmo\nb)Tulkas\nc)Orome\nd)Melkor", "d"])  # 19
lotrquiz.append(["LOTR: What is the name of the black gate of Mordor?", "a)Gondolin\nb)Morannon\nc)Edoras\nd)Nargothrond", "b"])  # 20
score = (0)  # This variable contains the user's score for the quiz.
for lotrloop in range(20):  # Everytime this loop runs the quiz gives the user the the first and the next questions.
    lotrrand = randint(0, len(lotrquiz) - 1)  # This creates the random number so questions can randomly be asked.
    print("")  # This prints a new line.
    print(str(lotrloop + 1) + ") " + lotrquiz[lotrrand][0])  # This prints a new question everytime the loop is run.
    print(lotrquiz[lotrrand][1])  # This prints the choices.
    userinput1 = input().lower()  # This asks the user to input their answer.
    if userinput1 == lotrquiz[lotrrand][2]:  # This increases the score depending if the user gets a question correct.
        print("Correct!")  # This prints correct if the user gets the question correct.
        score = (score + 1)  # If the user gets a question correct the score goes up by one.
    else:  # This controls the print statement.
        print("Incorrect! The correct answer was " + lotrquiz[lotrrand][2] + ".")  # This prints incorrect and tells the user the correct answer if the user answers the question incorrectly.
    lotrquiz.remove(lotrquiz[lotrrand])  # This makes sure a question doesn't repeat itself in the quiz.
print("")  # This prints a new line.
print(("Score: ") + str(score))  # This prints the score at the end of the quiz.