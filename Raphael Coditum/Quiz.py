from random import randint
print("Welcome to my quiz game.")
randomgame=randint(1,100)
print("Try to guess my favorite number. Choose a number from 1-100.")
guess=int(input())
numofguess=1
while not guess==randomgame:
    if guess<=randomgame:
        print("You guessed too low!")
    if guess>=randomgame:
        print("You guessed too high!")
    if not guess==randomgame:
        print("You didn't guess my favorite number.")
        print("Number of guesses ", end="")
        print(numofguess)
        guess=int(input())
        numofguess=numofguess+1
    if guess==randomgame:
        print("Number of guesses ", end="")
        print(numofguess)
print("You guessed my favorite number!")